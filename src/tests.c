#include "tests.h"

#define BASE_BLOCK_SIZE 1000
#define BASE_HEAP_SIZE 8192


void test1(void) {
    printf("Test 1\n");
    void* heap = heap_init(BASE_HEAP_SIZE);
    debug_heap(stdout, heap);
    void* block1 = _malloc(BASE_BLOCK_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL || block1 == NULL) {
        printf("Test 1 FAILED\n");
        return;
    }
    _free(block1);
    debug_heap(stdout, heap);
    munmap(heap, BASE_HEAP_SIZE);
    printf("Test 1 OK\n");
}

void test2(void) {
    printf("Test 2\n");
    void* heap = heap_init(BASE_HEAP_SIZE);
    debug_heap(stdout, heap);
    void* block1 = _malloc(BASE_BLOCK_SIZE);
    void* block2 = _malloc(BASE_BLOCK_SIZE * 2);
    debug_heap(stdout, heap);
    if (heap == NULL || block1 == NULL || block2 == NULL) {
        printf("Test 2 FAILED\n");
        return;
    }
    _free(block1);
    debug_heap(stdout, heap);
    munmap(heap, BASE_HEAP_SIZE);
    printf("Test 2 OK\n");
}

void test3(void) {
    printf("Test 3\n");
    void* heap = heap_init(BASE_HEAP_SIZE);
    debug_heap(stdout, heap);
    void* block1 = _malloc(BASE_BLOCK_SIZE);
    void* block2 = _malloc(BASE_BLOCK_SIZE * 2);
    void* block3 = _malloc(BASE_BLOCK_SIZE * 3);
    debug_heap(stdout, heap);
    if (heap == NULL || block1 == NULL || block2 == NULL || block3 == NULL) {
        printf("Test 3 FAILED\n");
        return;
    }
    _free(block1);
    _free(block2);
    debug_heap(stdout, heap);
    munmap(heap, BASE_HEAP_SIZE);
    printf("Test 3 OK\n");
}

void test4(void) {
    printf("Test 4\n");
    void* heap = heap_init(BASE_HEAP_SIZE);
    debug_heap(stdout, heap);
    void* block1 = _malloc(BASE_HEAP_SIZE);
    void* block2 = _malloc(BASE_HEAP_SIZE);
    debug_heap(stdout, heap);
    if (heap == NULL || block1 == NULL || block2 == NULL) {
        printf("Test 4 FAILED\n");
        return;
    }
    _free(block1);
    _free(block2);
    debug_heap(stdout, heap);
    munmap(heap, BASE_HEAP_SIZE);
    printf("Test 4 OK\n");
}

void test5(void) {
    printf("Test 5\n");
    void* heap = heap_init(BASE_HEAP_SIZE);
    debug_heap(stdout, heap);
    void* block1 = _malloc(BASE_HEAP_SIZE * 2);
    debug_heap(stdout, heap);
    if (heap == NULL || block1 == NULL) {
        printf("Test 5 FAILED\n");
        return;
    }
    _free(block1);
    debug_heap(stdout, heap);
    munmap(heap, BASE_HEAP_SIZE);
    printf("Test 5 OK\n");
}

