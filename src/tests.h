#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"

void test1(void);
void test2(void);
void test3(void);
void test4(void);
void test5(void);

#endif
